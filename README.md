## BoB-Linux Project
![bob_bi_basic](https://user-images.githubusercontent.com/67176669/91658741-dccce200-eb05-11ea-8cd4-6dc2c08cc8f9.jpg)
### TEAM : Black Moon
#### Team Member
- Mentor
  - 주 멘토 : 조성재 멘토님
  - 부 멘토 : 조민재 멘토님
- PL
  - 권현준
- Mentee
  - PM : 김청준
  - Member : 김기서, 노무승, 백송선, 이안나
  
#### 주제
- 다양한 리눅스 배포판을 분석하고 목적(보안 관제, 모의해킹, 보안 시스템, 서버 등)에 맞는 리눅스 배포판 개발 
- 보안을 공부하는 누구나 쉽고 빠르게 필요한 툴을 설치할 수 있게 함